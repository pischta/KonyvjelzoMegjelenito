package KönyvjelzőMegjelenítő;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


public class Modell {
  private DefaultMutableTreeNode faGyökér;//= new DefaultMutableTreeNode("Könyvjelzők");
  DefaultTreeModel dtm;// = new DefaultTreeModel(faGyökér);
  private String könyvjelzőkString;
  private JSONObject könyvjelzőkJSON;

  public Modell(){
    könyvjelzőkString = könyvjelzőkStringbe();
    //System.out.println(könyvjelzőkString);
    try {
      könyvjelzőkJSON = new JSONObject(könyvjelzőkString);
      //System.out.println(könyvjelzőkJSON.toString(2));
    } catch (JSONException ex) {
      Logger.getLogger(Könyvjelzők.class.getName()).log(Level.SEVERE, null, ex);
    }
    faGyökér=könyvjelzőfátÉpít(könyvjelzőkJSON);
    dtm= new DefaultTreeModel(faGyökér);
    faGyökér.setUserObject("Könyvjelzők");
  }
  
  private String könyvjelzőkStringbe() {
    StringBuilder bookmarks = new StringBuilder();
    //File könyvjelzőkFájl = new File("bookmarks-2017-04-05.json");
    File könyvjelzőkFájl = new File("bookmarks-2017-04-10.json");
    try {
      BufferedReader br = new BufferedReader(new FileReader(könyvjelzőkFájl));
      try {
        String egySor = br.readLine();
        while (egySor != null) {
          bookmarks.append(egySor);
          egySor = br.readLine();
        }
      } catch (IOException e) {
        System.out.println(e.getMessage());
      }
    } catch (FileNotFoundException ex) {
      Logger.getLogger(Könyvjelzők.class.getName()).log(Level.SEVERE, null, ex);
    }
    return bookmarks.toString();
  }

  
  private DefaultMutableTreeNode könyvjelzőfátÉpít(JSONObject jo) {
    String title="";
    DefaultMutableTreeNode ág;
    try {
      if(jo.has("title")){
        if (jo.getString("title").equals("")){
          title="?";
        }else{
          title=jo.getString("title");
        }
      }
      //ha van type key, akkor azt beleteszem a typeGyűjteménybe
      if (jo.getString("type").equals("text/x-moz-place")) {//akkor link
        DefaultMutableTreeNode levél=new DefaultMutableTreeNode(title);
        ág=new DefaultMutableTreeNode(levél);
        if(jo.has("uri"))
          ág.add(new DefaultMutableTreeNode(jo.getString("uri")));
        else if(jo.has("iconuri"))
          ág.add(new DefaultMutableTreeNode(jo.getString("iconuri")));
      } else if (jo.getString("type").equals("text/x-moz-place-separator")) {//akkor elválasztó
        DefaultMutableTreeNode levél=new DefaultMutableTreeNode("-----------------------------");
        ág=new DefaultMutableTreeNode(levél);
      } else if (jo.getString("type").equals("text/x-moz-place-container")) {//akkor tároló 
        DefaultMutableTreeNode levél=new DefaultMutableTreeNode(title);
        ág=new DefaultMutableTreeNode(levél);
      } else{
        DefaultMutableTreeNode levél=new DefaultMutableTreeNode("Egyéb...");
        ág=new DefaultMutableTreeNode(levél);
      }
    } catch (JSONException ex) {
      if(jo.has("value")){
        DefaultMutableTreeNode levél;
        try {
          levél = new DefaultMutableTreeNode("Leírás: "+jo.getString("value"));
          ág=new DefaultMutableTreeNode(levél);
        } catch (JSONException ex1) {
          try {
            //Ide akkor jut, ha a "value"-ban szám van:
            levél = new DefaultMutableTreeNode("Leírás: "+jo.getInt("value"));
            ág=new DefaultMutableTreeNode(levél);
          } catch (JSONException ex2) {
            //Remélhetőleg ide nem jut soha:
            levél=new DefaultMutableTreeNode("Egyéb, kivétel ágon...");
            ág=new DefaultMutableTreeNode(levél);
          }
        }
      }else{
        DefaultMutableTreeNode levél=new DefaultMutableTreeNode("Egyéb, kivétel ágon...");
        ág=new DefaultMutableTreeNode(levél);

      }
    }
    //végigmegy a kulcsokon
    //ha a kulcs egy JSONObject, akkor rekurzívan meghívom a függvényt vele
    //ha JSONArray, akkor végigmegyek a tömb elemein
    JSONArray jaKulcsok= jo.names();
    for (int i = 0; i < jaKulcsok.length(); i++) {
      JSONArray ja;
      try {
        String kulcs=jaKulcsok.getString(i);
        ja=jo.getJSONArray(kulcs);//Ha nem tartozik tömb a kulcshoz, akkor kivételt dob, így nem folytatja a blokkot, ezért nem kell a null-t vizsgálni
        for (int j = 0; j < ja.length(); j++) {
              ág.add(könyvjelzőfátÉpít(ja.getJSONObject(j)));
        }
      } catch (JSONException ex) {
        //Nem gond, ha nem talál kulcsot
      }
    }
    return ág;
  }
}
