package KönyvjelzőMegjelenítő;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTree;

public class Ablak extends JFrame {
  private JScrollPane jScrollPanel;
  private JTree tKönyvjelzőkFa = new javax.swing.JTree();
  public Ablak(final Modell modell){
    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setTitle("Könyvjelzők");
    setSize(800, 600);
    setLocationRelativeTo(this);
    jScrollPanel = new JScrollPane();
    tKönyvjelzőkFa.setModel(null);
    tKönyvjelzőkFa.setModel(modell.dtm);
    jScrollPanel.setViewportView(tKönyvjelzőkFa);
    add(jScrollPanel);
    setVisible(true);
  }
}
